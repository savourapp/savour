angular.module('app.controllers', ['ionic','ngCordova','ionic-timepicker','app.directives'])

.controller('promotionsCtrl', ['$scope', '$stateParams','$state','$interval','$http','$ionicModal', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$state,$interval,$http,$ionicModal) {


  console.log(localStorage.getItem('Session'))
  $scope.check = function(hotelId){
    // a.stopPropagation();
    // $state.go("createAccount");

    var localStorage = window.localStorage;

    if(localStorage.getItem("Session")==null)
    {
      $state.go("createAccount");
    }
    else{
      // alert(hotelId)
      $scope.hotelId = hotelId;
      openModal();
}



}

$scope.data = {};
      $ionicModal.fromTemplateUrl('modal.html', function(modal) {
        $scope.gridModal = modal;
      }, {
        scope: $scope,
        animation: 'slide-in-up'
      });
  // open video modal
  function openModal () {
    // alert($scope.hotelId)
    $scope.data.selected = $scope.hotelId;
    $scope.gridModal.show();
  };
  // close video modal
  $scope.closeModal = function() {
    $scope.gridModal.hide();
  };
  //Cleanup the video modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.gridModal.remove();
  });

$http.get('js/hotels.json').success(function(data) {
  
  $scope.hotels = data;  
    // $scope.whichartist = $state.params.aId;
    $scope.whichhotel = $state.params.aId;

    $scope.doRefresh =function() {
      $http.get('js/hotels.json').success(function(data) {
        $scope.hotels = data;
        $scope.$broadcast('scroll.refreshComplete'); 
      });
    }

  });

 /* $scope.callHotelDetails = function(){
    $state.go("hotelDetails");
  }*/




  var countDownDate = new Date("july 31, 2017 15:18:25").getTime();
  var x = $interval(function() {
    var now = new Date().getTime();
    var distance = countDownDate - now;
    $scope.days = Math.floor(distance / (1000 * 60 * 60 * 24));
    $scope.hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    $scope.minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    $scope.seconds = Math.floor((distance % (1000 * 60)) / 1000);
    if (distance < 0) {
      clearInterval(x);
      document.getElementById("timer").innerHTML = "EXPIRED";
    }
  }, 1000);

}])


.controller('restaurantsCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

.controller('allvendorsCtrl', ['$scope', '$stateParams','$http', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $http) {

  $http.get('js/hotels.json').success(function(data) {
    $scope.hotels = data;  
    // $scope.whichartist = $state.params.aId;
    // $scope.whichhotel = $state.params.Id;

    $scope.doRefresh =function() {
      $http.get('js/hotels.json').success(function(data) {
        $scope.hotels = data;
        $scope.$broadcast('scroll.refreshComplete'); 
      });
    }


  });

}])

.controller('walletCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

.controller('redeemedCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

.controller('saveDealsCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])




.controller('menuCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

.controller('CreateAccountCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

.controller('ManagerProfileCtrl', ['$scope', '$stateParams','$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$state) {

 $scope.logout = function() {
  localStorage.clear();
  $state.go("login");
}


}])

.controller('loginCtrl', 

 function($scope, $state, $location) {


  $scope.login = function(name, pass) {

    if (name == "manager" && pass == "password") {
      alert("Success");
      var localStorage = window.localStorage; 
      localStorage.clear();
      localStorage.setItem("Session", "login");
      $location.path('/managerTab/manageAccount'); 

    } else if (name == "user" && pass == "password") {
      alert("Success");
      var localStorage = window.localStorage; 
      localStorage.clear();
      localStorage.setItem("Session", "login");
      $location.path('/userTab/promotions');

    } 

    else {
      alert("Please Check Your Credentials");
    }


  }

})


.controller('happyHoursCtrl',['$scope','ionicTimePicker','$location',function($scope,ionicTimePicker,$location) {


  $scope.gbc1 = function() {
    var ipObj1 = {
                  callback: function (val) {      //Mandatory
                    if (typeof (val) === 'undefined') {
                      console.log('Time not selected');
                    } else {
                      var selectedTime = new Date(val * 1000);
                      //console.log('Selected epoch is : ', val, 'and the time is ', selectedTime.getUTCHours(), 'H :', selectedTime.getUTCMinutes(), 'M');
                      var time = selectedTime.getUTCHours()+ 'H :' +' '+  selectedTime.getUTCMinutes()+ 'M';
                     //alert(time);
                     document.querySelector('.results').innerHTML =  'From Time ' + (time);
                   }
                 },
                  inputTime: 50400,   //Optional
                  format: 12,         //Optional
                  step: 15,           //Optional
                  setLabel: 'Set'    //Optional
                };

                ionicTimePicker.openTimePicker(ipObj1);

              }

              $scope.gbc2 = function() {
                var ipObj1 = {
                  callback: function (val) {      //Mandatory
                    if (typeof (val) === 'undefined') {
                      console.log('Time not selected');
                    } else {
                      var selectedTime = new Date(val * 1000);
                      //console.log('Selected epoch is : ', val, 'and the time is ', selectedTime.getUTCHours(), 'H :', selectedTime.getUTCMinutes(), 'M');
                      var time = selectedTime.getUTCHours()+ 'H :' +' '+  selectedTime.getUTCMinutes()+ 'M';
                     //alert(time);
                     document.querySelector('.resultsTwo').innerHTML =  'To Time ' + (time);
                   }
                 },
                  inputTime: 50400,   //Optional
                  format: 12,         //Optional
                  step: 15,           //Optional
                  setLabel: 'Set'    //Optional
                };

                ionicTimePicker.openTimePicker(ipObj1);

              }

            }])

.controller('CreatePromotionCtrl',['$scope', '$stateParams','$state','ionicTimePicker','$location','$interval', 

  function($scope,$stateParams,$state,ionicTimePicker,$location,$interval){

    var localStorage = window.localStorage;
    if(localStorage.getItem("Session")!="login")
    {
      $state.go("login");
    }

    $scope.logout = function() {
      localStorage.clear();
      $state.go("login");
    }

    var countDate = new Date("july 31, 2017 15:18:25").getTime();

// Update the count down every 1 second
var xy = $interval(function() {

    // Get todays date and time
    var now1 = new Date().getTime();
    
    // Find the distance between now an the count down date
    var distance1 = countDate - now1;
    
    // Time calculations for days, hours, minutes and seconds
    $scope.daysc = Math.floor(distance1 / (1000 * 60 * 60 * 24));
    $scope.hoursc = Math.floor((distance1 % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    $scope.minutesc = Math.floor((distance1 % (1000 * 60 * 60)) / (1000 * 60));
    //var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
    // Output the result in an element with id="demo"
  /*  document.getElementById("timerPromotion").innerHTML =  + days + "d "
  + hours + "h " + minutes+ "m ";*/
  
    // If the count down is over, write some text 
   /* if (distance < 0) {
        clearInterval(x);
        document.getElementById("timerPromotion").innerHTML = "EXPIRED";
      }*/
    }, 1000);

}]
)


.controller('ManageAccountCtrl', function($scope,ionicTimePicker,$location) {

 var localStorage = window.localStorage;
 if(localStorage.getItem("Session")!="login")
 {
  $location.path('/login');
}

$scope.gbc = function() {
  navigator.camera.getPicture(success, function(message) {
    alert('Upload picture cancelled!!');
  }, {
    quality: 50,
    destinationType: navigator.camera.DestinationType.FILE_URI,
    sourceType: navigator.camera.PictureSourceType.camera,
    allowEdit: true
  });
}

function success(imageURI) {
  var smallImage = document.getElementById('uploadimg');
  smallImage.src = imageURI;

    }/*
    $scope.gbc1 = function() {
      var ipObj1 = {
                  callback: function (val) {      //Mandatory
                    if (typeof (val) === 'undefined') {
                      console.log('Time not selected');
                    } else {
                      var selectedTime = new Date(val * 1000);
                      //console.log('Selected epoch is : ', val, 'and the time is ', selectedTime.getUTCHours(), 'H :', selectedTime.getUTCMinutes(), 'M');
                      var time = selectedTime.getUTCHours()+ 'H :' +' '+  selectedTime.getUTCMinutes()+ 'M';
                     //alert(time);
                     document.querySelector('.results').innerHTML =  'From Time ' + (time);
                   }
                 },
                  inputTime: 50400,   //Optional
                  format: 12,         //Optional
                  step: 15,           //Optional
                  setLabel: 'Set'    //Optional
                };

                ionicTimePicker.openTimePicker(ipObj1);

              }

              $scope.gbc2 = function() {
                var ipObj1 = {
                  callback: function (val) {      //Mandatory
                    if (typeof (val) === 'undefined') {
                      console.log('Time not selected');
                    } else {
                      var selectedTime = new Date(val * 1000);
                      //console.log('Selected epoch is : ', val, 'and the time is ', selectedTime.getUTCHours(), 'H :', selectedTime.getUTCMinutes(), 'M');
                      var time = selectedTime.getUTCHours()+ 'H :' +' '+  selectedTime.getUTCMinutes()+ 'M';
                     //alert(time);
                     document.querySelector('.resultsTwo').innerHTML =  'To Time ' + (time);
                   }
                 },
                  inputTime: 50400,   //Optional
                  format: 12,         //Optional
                  step: 15,           //Optional
                  setLabel: 'Set'    //Optional
                };

                ionicTimePicker.openTimePicker(ipObj1);

              }*/
              $scope.logout = function() {
                localStorage.clear();
                $location.path('/login');
              }


            })

   .controller('userProfileCtrl', ['$scope', '$stateParams','$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $state) {

  $scope.logout = function() {
    localStorage.clear();
    $state.go("login");
  }

}])

