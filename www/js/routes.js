angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

 // LOGIN 

 .state('login', {
  url: '/login',
  templateUrl: 'templates/login.html',
  controller: 'loginCtrl'
})



//SIDE MENU
/*.state('menu', {
  url: '/menu',
  abstract: true,
  templateUrl: 'templates/menu.html',
  controller: 'menuCtrl'
})

.state('menu.promotions', {
  url: '/promotions',
  views: {
    'menuContent': {
      templateUrl: 'templates/promotions.html',
      controller: 'promotionsCtrl'
    }
  }
})


.state('noPromotion', {
  url: '/noPromotion',
  templateUrl: 'templates/noPromotion.html'
})

.state('hotelDetails', {
  url: '/hotelDetails',

  templateUrl: 'templates/hotelDetails.html'
})

.state('menu.restaurants', {
  url: '/restaurants',
  views: {
    'menuContent': {
      templateUrl: 'templates/restaurants.html',
      controller: 'restaurantsCtrl'
    }
  }
})

.state('menu.wallet', {
  url: '/wallet',
  views: {
    'menuContent': {
      templateUrl: 'templates/wallet.html',
      controller: 'walletCtrl'
    }
  }
})*/

// WALLET SCREEN PART 
/*.state('menu.saveDeals', {
  url: '/saveDeals',
  views:
  {
    'menuContent': {
      templateUrl: 'templates/saveDeals.html'
    }
  }
})

.state('menu.redeemed', {
  url: '/redeemed',
  views:
  {
    'menuContent': {
      templateUrl: 'templates/redeemed.html'
    }
  }
})


.state('menu.profile', {
  url: '/profile',
  views: {
    'menuContent': {
      templateUrl: 'templates/profile.html',
      controller: 'profileCtrl'
    }
  }
})

.state('menu.userInterfacehome', {
  url: '/userInterfacehome',
  views: {
    'menuContent': {
      templateUrl: 'templates/userInterfaceHome.html',
      controller: 'profileCtrl'
    }
  }
})*/

// .state('userInterfaceHome', {
//   url: '/userInterfaceHome',
//       templateUrl: 'templates/userInterfaceHome.html'
//   }
// })

//USER TABS
.state('UserTabController', {
  url: '/userTab',
  templateUrl: 'templates/UserTabController.html',
  abstract:true
})

.state('UserTabController.promotions', {
  url: '/promotions',
  views: {
    'promotions': {
      templateUrl: 'templates/promotions.html',
      controller: 'promotionsCtrl'
    }
  }
})

.state('UserTabController.hotelDetails', {
  url: '/promotions/:aId',
  views: {
    'promotions': {
      templateUrl: 'templates/hotelDetails.html',
      controller: 'promotionsCtrl'
    }
  }
})

.state('voucher', {
  url: '/voucher',
  templateUrl: 'templates/voucher.html'
})

.state('UserTabController.allvendors', {
  url: '/allvendors',
  views: {
    'all-vendors': {
      templateUrl: 'templates/restaurants.html',
      controller: 'allvendorsCtrl'
    }
  }
})

.state('UserTabController.wallet', {
  url: '/wallet',
  views: {
    'wallet': {
      templateUrl: 'templates/wallet.html',
      controller: 'walletCtrl'
    }
  }
})

.state('UserTabController.redeemed', {
  url: '/redeemed',
  views: {
    'wallet': {
      templateUrl: 'templates/redeemed.html',
    }
  }
})

.state('UserTabController.saveDeals', {
  url: '/saveDeals',
  views: {
    'wallet': {
      templateUrl: 'templates/saveDeals.html',
    }
  }
})

.state('UserTabController.userProfile', {
  url: '/userProfile',
  views: {
    'promotions': {
      templateUrl: 'templates/userProfile.html',
      controller: 'userProfileCtrl'
    }
  }
})



//MangerMenu


.state('ManagerTabController', {
  url: '/managerTab',
  templateUrl: 'templates/ManagerTabController.html',
  abstract:true
})

.state('ManagerTabController.manageAccount', {
  url: '/manageAccount',
  views: {
    'manage-account': {
      templateUrl: 'templates/manageAccount.html',
      controller: 'ManageAccountCtrl'
    }
  }
})

.state('ManagerTabController.happyHours', {
  url: '/happyHours',
  views : {
   'manage-account':{
    templateUrl: 'templates/happyHours.html',
    controller: 'happyHoursCtrl'
  }
}
})

.state('ManagerTabController.happyTimes', {
  url: '/happyTimes',
  views : {
   'manage-account':{
    templateUrl: 'templates/happyTimes.html',
    controller: 'happyHoursCtrl'
  }
}
})


.state('ManagerTabController.createPromotion', {
  url: '/createPromotion',
  views: {
    'create-promotion': {
      templateUrl: 'templates/createPromotion.html',
      controller: 'CreatePromotionCtrl'
    }
  }
})


.state('createAccount', {
  url: '/createAccount',
  templateUrl: 'templates/createAccount.html',
  controller: 'CreateAccountCtrl'
})

.state('ManagerTabController.managerProfile', {
  url: '/managerProfile',
  views:{
    'manage-account':{
      templateUrl: 'templates/managerProfile.html',
      controller: 'ManagerProfileCtrl'
    }
  }
})



$urlRouterProvider.otherwise("userTab/promotions")


});